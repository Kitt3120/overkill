/*
 * Copyright (c) 21.09.19, 19:58 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill.gamestate;

import de.kitt3120.overkill.Overkill;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class GameStateBehaviour implements Listener {

    private GameStateType gameStateType;

    private boolean prepared = true;
    private boolean preparedAsync = true;

    public GameStateBehaviour(GameStateType gameStateType) {
        this.gameStateType = gameStateType;

        if(needsPreparation()) {
            prepared = false;
            preparedAsync = false;
        }
    }

    //Whether or not this GameState needs preparation
    public abstract boolean needsPreparation();

    //Used to do heavy preparation work on sync and async task before switching to this GameState, like generating blocks
    public void prepare() {

        new BukkitRunnable() {
            @Override
            public void run() {
                doPreparationAsync();
                preparedAsync = true;
            }
        }.runTaskAsynchronously(Overkill.getInstance());

        doPreparation();
        prepared = true;
    }

    //Sync preparation work
    //To be overwritten by sub-class
    private void doPreparation() {}

    //Async preparation work
    //To be overwritten by sub-class
    private void doPreparationAsync() {}

    //Is preparation done?
    public boolean isReady() {
        return prepared && preparedAsync;
    }

    /*
        Used for special behaviours between states,
        for example to teleport the players from one point to another.
        Called after previous GameStateBehaviour's onDisable() and before this one's onEnable()
     */
    public void doTransition() {}

    //Used to register events, start tasks, etc
    public void onEnable() {
        Overkill.getInstance().getServer().getPluginManager().registerEvents(this,Overkill.getInstance());
    }

    public void onDisable() {
        HandlerList.unregisterAll(this);
    }

    public GameStateType getGameStateType() {
        return gameStateType;
    }
}
