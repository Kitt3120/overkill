/*
 * Copyright (c) 21.09.19, 19:58 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill.gui;

import de.kitt3120.overkill.Overkill;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class GUI implements Listener {

	private Inventory inventory;
	private String title;
	private int size;
	private HashMap<Integer, GUIItem> guiItems;
	
	private BukkitTask updateTask;
	private boolean shouldUpdate;
	private long updateDelay;
	
	private boolean eventsRegistered = false;
	
	private boolean needsUpdate;
	
	public GUI(String title, int size) {
		this(title, size, false, 10L);
	}
	
	public GUI(String title, int size, boolean shouldUpdate) {
		this(title, size, shouldUpdate, 10L);
	}
	
	public GUI(String title, int size, boolean shouldUpdate, long updateDelay) {
		guiItems = new HashMap<>();
		
		this.shouldUpdate = shouldUpdate;
		this.updateDelay = updateDelay;
		
		this.title = title;
		this.size = size;
				
		needsUpdate = false;
		
		registerEvents();
		
		refresh();
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(e.getInventory().equals(inventory) && inventory.getViewers().contains(e.getWhoClicked()) && e.getWhoClicked() instanceof Player && e.getCurrentItem() != null && !e.getCurrentItem().getType().equals(Material.AIR)) {
			e.setCancelled(true);
			click(e);
		}
	}
	
	@EventHandler
	public void onCloseEvent(InventoryCloseEvent e) {
		if(e.getInventory().equals(inventory)) onClose(e);
	}
	
	public void click(InventoryClickEvent e) {
		if(!e.getAction().name().toLowerCase().startsWith("pickup")) return;
		GUIItem item = guiItems.get(e.getSlot());
		if(item != null && item.hasAction()) item.action(e);
	}
	
	public void onClose(InventoryCloseEvent e) {}
	
	public void close() {
		for (HumanEntity human : inventory.getViewers()) human.closeInventory();
		stopUpdates();
	}
	
	public void open(HumanEntity human) {
		if(!inventory.getViewers().contains(human)) {
			if(!eventsRegistered) registerEvents();
			human.openInventory(inventory);
			if(!isStarted()) startUpdates();
		}
	}
	
	public void addItem(GUIItem item) {
		addItem(getNextSlot(), item);
	}

	public void addItem(int slot, GUIItem item) {
		if(!guiItems.containsKey(slot) && !guiItems.containsValue(item)) {
			guiItems.put(slot, item);
			refresh();
		}
	}
	
	public void removeItem(int slot) {
		guiItems.remove(slot);
		refresh();
	}

	public boolean hasItem(int slot) {
		return guiItems.containsKey(slot);
	}

	public boolean hasItem(GUIItem item) {
		return guiItems.containsValue(item);
	}
	
	private void refresh() {
		ArrayList<HumanEntity> oldViewers = new ArrayList<>();
		if(inventory != null) oldViewers.addAll(inventory.getViewers());
		
		if (size <= 0) inventory = Bukkit.createInventory(null, getRecommendedSize(), title);
		else {
			if(size % 9 != 0) while(size % 9 != 0) size ++;
		    
			inventory = Bukkit.createInventory(null, size, title);
		}
		
		for (GUIItem item : guiItems.values()) item.onUpdate();
		needsUpdate = false;
		
		for (Entry<Integer, GUIItem> entry : guiItems.entrySet()) inventory.setItem(entry.getKey(), entry.getValue().getItem());
		
		for (HumanEntity humanEntity : oldViewers) humanEntity.openInventory(inventory);
	}

	public void setSize(int size) {
		this.size = size;
		refresh();
	}
	
	public void setTitle(String title) {
		this.title = title;
		needsUpdate = true;
	}
	
	private boolean isStarted() {
		return updateTask != null;
	}
	
	private void startUpdates() {
		if(shouldUpdate && updateTask == null) {
			updateTask = new BukkitRunnable() {
				
				@Override
				public void run() {
					if(!isStarted() || (inventory != null && inventory.getViewers().size() == 0)) {
						stopUpdates();
						cancel();
					} else {
						for (GUIItem item : guiItems.values()) item.onTick();
						if(needsUpdate()) refresh();
					}
				}
			}.runTaskTimer(Overkill.getInstance(), 1, updateDelay);
		}
	}
	
	private void stopUpdates() {
		if(updateTask != null) {
			updateTask = null;
			unregisterEvents();
		}
	}
	
	private void registerEvents() {
		if(!eventsRegistered) {
			Overkill.getInstance().getServer().getPluginManager().registerEvents(this, Overkill.getInstance());
			eventsRegistered = true;
		}
	}
	
	private void unregisterEvents() {
		if(eventsRegistered) {
			HandlerList.unregisterAll(this);
			eventsRegistered = false;
		}
	}
	
	private int getRecommendedSize() {
		int maxKey = 0;
		for(int k : guiItems.keySet()) if(maxKey < k) maxKey = k;
		maxKey++;
		
		while(maxKey % 9 != 0) maxKey ++;
		return maxKey;
	}
	
	private int getNextSlot() {
		for(int i = 0; i < inventory.getSize(); i++) if(!guiItems.containsKey(i)) return i;
		return inventory.getSize();
	}
	
	private boolean needsUpdate() {
		boolean itemNeedsUpdate = false;
		for (GUIItem guiItem : guiItems.values()) if(guiItem.needsUpdate()) itemNeedsUpdate = true;
		return needsUpdate || itemNeedsUpdate;
	}

}
