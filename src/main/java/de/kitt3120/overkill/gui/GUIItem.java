/*
 * Copyright (c) 21.09.19, 19:58 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill.gui;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class GUIItem {
	
	private ItemStack item;
	private GUIItemAction action;
	
	private boolean needsUpdate;
	
	public GUIItem(Material material, int count, String name, String[] lore, GUIItemAction action) {
		this(material, count, (short) 0, name, lore, action);
	}
	
	public GUIItem(Material material, int count, String name, String[] lore) {
		this(material, count, (short) 0, name, lore, null);
	}
	
	public GUIItem(Material material, int count, short damage, String name, String[] lore) {
		this(material, count, damage, name, lore, null);
	}
	
	public GUIItem(Material material, int count, short damage, String name, String[] lore, GUIItemAction action) {
		this.item = new ItemStack(material, count, damage);
		this.action = action;
		
		setItemName(name);
		setItemLore(lore);
		
		needsUpdate = false;
	}
	
	public GUIItem(ItemStack item) {
		this(item, null);
	}
	
	public GUIItem(ItemStack item, GUIItemAction action) {
		this.item = item;
		this.action = action;
	}
	
	public void setItemType(Material material) {
		item.setType(material);
		
		needsUpdate = true;
	}
	
	public void setItemName(String name) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		
		needsUpdate = true;
	}
	
	public void setItemLore(String[] lore) {
		if(lore == null) {
			ItemMeta meta = item.getItemMeta();
			meta.setLore(null);
			item.setItemMeta(meta);
			
			needsUpdate = true;
		}
		else setItemLore(Arrays.asList(lore));
	}
	
	public void setItemLore(List<String> lore) {
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lore);
		item.setItemMeta(meta);
		
		needsUpdate = true;
	}
	
	public void onTick() {} //For overriding, each tick
	
	public void onUpdate() { //For overriding with super(), each update (a tick but only when an update is actually needed)
		needsUpdate = false;
	}
	
	public boolean needsUpdate() {
		return needsUpdate;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public boolean hasAction() {
		return action != null;
	}
	
	public GUIItemAction getAction() {
		return action;
	}
	
	public void setAction(GUIItemAction action) {
		this.action = action;
	}
	
	public void action(InventoryClickEvent e) {
		if(hasAction()) action.onAction(e);
	}

}
