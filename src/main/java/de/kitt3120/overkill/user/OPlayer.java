/*
 * Copyright (c) 27.09.19, 08:35 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill.user;

import de.kitt3120.overkill.Overkill;
import de.kitt3120.overkill.statistics.Stattracker;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

public class OPlayer {

    private UUID uuid;
    private Stattracker stattracker;

    public OPlayer(UUID uuid) {
        this.uuid = uuid;
        this.stattracker = new Stattracker(this);
    }

    public UUID getUuid() {
        return uuid;
    }

    public Player toPlayer() {
        return Overkill.getInstance().getServer().getPlayer(uuid);
    }

    public OfflinePlayer toOfflinePlayer() {
        return Overkill.getInstance().getServer().getOfflinePlayer(uuid);
    }

    public Stattracker getStattracker() {
        return stattracker;
    }
}
