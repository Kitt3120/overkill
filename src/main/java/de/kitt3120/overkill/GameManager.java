/*
 * Copyright (c) 21.09.19, 19:58 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill;

import de.kitt3120.overkill.gamestate.GameStateBehaviour;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class GameManager implements Listener {

    private GameStateBehaviour currentGameStateBehaviour;

    public void register() {
        Overkill.getInstance().getServer().getPluginManager().registerEvents(this,Overkill.getInstance());
    }

    public void unregister() {
        HandlerList.unregisterAll(this);
    }

    public GameStateBehaviour getCurrentGameStateBehaviour() {
        return currentGameStateBehaviour;
    }

    public void switchGameStateBehaviour(GameStateBehaviour gameStateBehaviour) {
        if(gameStateBehaviour.needsPreparation())
            gameStateBehaviour.prepare();

        //New thread so we would not sleep the main thread when using Thread.sleep() while waiting for the preparation
        new Thread(() -> {
            while(!gameStateBehaviour.isReady()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Overkill.getInstance().getLogger().severe("[GameManager] Failed to wait for GameStateBehaviour to finish preparation!");
                    break;
                }
            }

            /*
                As this Task will run asynchronously to the main thread bukkit is using,
                we will call the code through a new sync scheduler task.
                This is similar to invoking.
             */
            new BukkitRunnable() {
                @Override
                public void run() {
                    if(currentGameStateBehaviour != null)
                        currentGameStateBehaviour.onDisable();

                    currentGameStateBehaviour = gameStateBehaviour;
                    gameStateBehaviour.doTransition();
                    gameStateBehaviour.onEnable();
                }
            }.runTask(Overkill.getInstance());
        }).start();
    }
}
