/*
 * Copyright (c) 27.09.19, 08:35 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill.utils;

import de.kitt3120.overkill.Overkill;

import java.io.File;

public class FileManager {
	
private static File root;

	public static void setup() {
		root = Overkill.getInstance().getDataFolder();
	}

	public static File getFile(File dir, String name) {
		File toReturn = new File(dir, name);
		if(toReturn.getParentFile().mkdirs()) Overkill.getInstance().getLogger().info("[FileManager] Created file " + toReturn.getParentFile().getAbsolutePath() + " to access file " + name);
		return toReturn;
	}

	public static File getFile(String name) {
		return getFile(root, name);
	}

	public static boolean existsFile(String name) {
		return new File(root, name).exists();
	}
	
}

