/*
 * Copyright (c) 27.09.19, 08:35 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill.statistics;

import de.kitt3120.overkill.Overkill;
import de.kitt3120.overkill.user.OPlayer;
import de.kitt3120.overkill.utils.FileManager;

import java.io.*;
import java.util.HashMap;

public class Stattracker {

    private OPlayer oPlayer;
    private HashMap<String, Object> stats;

    public Stattracker(OPlayer oPlayer) {
        this.oPlayer = oPlayer;

        try {
            loadStats();
        } catch (IOException e) {
            e.printStackTrace();
            Overkill.getInstance().getLogger().severe("[Stattrracker] Failed to load stats of user " + oPlayer.toOfflinePlayer().getName() + ": IOException");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Overkill.getInstance().getLogger().severe("[Stattrracker] Failed to load stats of user " + oPlayer.toOfflinePlayer().getName() + ": ClassNotFoundException");
        }
    }

    private File getStatFile() {
        return FileManager.getFile("Statistics/" + oPlayer.getUuid().toString());
    }

    private boolean existsStatFile() {
        return FileManager.existsFile("Statistics/" + oPlayer.getUuid().toString());
    }

    public void loadStats() throws IOException, ClassNotFoundException {
        if(existsStatFile()) {
            ObjectInputStream objectInputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(getStatFile())));
            stats = (HashMap<String, Object>) objectInputStream.readObject();
            objectInputStream.close();
        } else stats = new HashMap<String, Object>();
    }

    public void saveStats() throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(getStatFile())));
        objectOutputStream.writeObject(stats);
        objectOutputStream.close();
    }

    public OPlayer getOPlayer() {
        return oPlayer;
    }
}
