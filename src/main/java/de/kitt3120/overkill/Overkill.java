/*
 * Copyright (c) 27.09.19, 08:35 Torben Schweren
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kitt3120.overkill;

import de.kitt3120.overkill.utils.FileManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Overkill extends JavaPlugin {

    private static Overkill instance;

    private GameManager gameManager;

    @Override
    public void onEnable() {
        super.onEnable();

        instance = this;
        FileManager.setup();

        gameManager = new GameManager();
        gameManager.register();
    }

    @Override
    public void onDisable() {
        super.onDisable();

        gameManager.unregister();
    }

    public static Overkill getInstance() {
        return instance;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

}
