# Overkill

**What's Overkill?**

Overkill is a PvP-Based game mode meant to be played for E-Sports or tournaments in general.
The game consists of different states the players play through.

---

### States

**Lobby state**

The players come together in a lobby, waiting for the game to begin.
While in the lobby, the players may find other team partners.
If players did not find any team partner after the timeout,
they will become team partners with another player left free.
While in the lobby, players may use lobby gadgets to have some fun ;)

**Loot state**

The individual teams will be spread across a big map to gather loot.
Chests will be filled with items randomly each game.
The teams can't see each other, can't take damage and mobs will not spawn.
Thus, this state is completely dedicated to gather loot.

**Build state**

Each team will get a 16x16 platform,
which they can use to build their base on.
They can build by using the items they collected in the loot state.
The team has to place a bed somewhere in their building which
will be used as the base's core.
The bed will be provided by the game for free in build state.
Once the time's up, their building will be saved.

**Battle state**

Along with their buildings, the teams will be spread across a big battle area.
The goal is to destroy the enemy base's cores while protecting your own one.
Dying results in a delayed respawn at your base's core.
If your base core gets destroyed, your team is out!
Last team standing wins.

**Ceremony state**

The winner team is presented.
Fireworks, particles and stuff!
Players may use lobby gadgets again :p
The server will restart itself after this state to restore the map.

---

### Special Items

Overkill will also add some special items on top of Minecraft's existing ones
like rocket launchers, grappling hooks and claymores.